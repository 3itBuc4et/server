package com.example.beautyspot.entity.enums;

public enum EStatus {
	IN_SALE,
	SOLD_OUT
}
